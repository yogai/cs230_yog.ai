
import cv2
import os
from datetime import datetime 
import numpy as np
import time

## Setup Path ##
def webcam(nrPics, newpath):
    

    #pathName = os.path.join(os.getcwd(),r'\capturedFrames')

    ####

    cam = cv2.VideoCapture(0) # 1 for webcam - 0 for back cam

    cv2.namedWindow("test")

    img_counter = 1   #Start at 1st pic
    timeToWait = 0.5 #seconds between pics

    time.sleep(timeToWait)
    #nrPics = int(input("Enter a number\n"))
    if(not str(nrPics).isdigit()):
        nrPics = 10             # If not an int, set it to ... pics default
        print("Defaults to " + str(nrPics) + " pictures")


    print("Will take " + str(nrPics) + " pictures")
    endPics = False
    
    for i in range(nrPics):
        print("Hit ESC to close / Hit Space to take [" + str(i+1) +"] /" + str(nrPics)+" Pictures")
            
        while endPics ==False:      #Boolean for when to end
            
            ret, frame = cam.read()            
            cv2.imshow("test", frame)
            if not ret:
                break

            k = cv2.waitKey(1)
            

            if k%256 == 27:
                # ESC pressed
                print("Escape hit, closing...")
                endPics = True
                break
            elif img_counter > nrPics:
                endPics = True
                print("All pictures taken!\n")
                break

            elif k%256 == 32:
                # SPACE pressed
                
                img_name = "opencv_frame_{}.png".format(img_counter)
                cv2.imwrite(os.path.join(newpath, img_name), frame)
                
                print("{} written!".format(img_name))
                
                print("Wait " + str(timeToWait) + " seconds ... Smile")
                time.sleep(timeToWait)

                img_counter += 1
                break
     


                    
        



    cam.release()

    cv2.destroyAllWindows()
    return
