# Wrapper file for cnn_multiclass.py
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from PIL import Image
from random import *
import h5py
import numpy as np
import json
from datetime import datetime 
import os
from cnn_multiclass import *
import time

#Create dictionary 
dictionary = dict()

#
dt = datetime.now().strftime('Day%m_%d_Time%H_%M_%S')


nrOfLoops = 10
for repeat in range(nrOfLoops):
	print("Starting sweep [" + str(repeat+1) + "] out of " + str(nrOfLoops) + " hyperparameter sweeps \n")

	nrOfRuns = 4

	#cnn_multiclass Function inputs
	epochs = 100
	gridSteps = 100


	
	

	higher_folder = 'saveFolder'
	sub_folder = 'Outputs'
	saveFolderName = os.path.join(os.path.join(higher_folder, sub_folder), dt)

	savePath = os.path.join(os.getcwd(),(saveFolderName))
	if not os.path.exists(savePath):
	    os.makedirs(savePath)


	plt.figure(1)



	for run in range(nrOfRuns):

		

		print("Running [" + str(run+1) + "] out of " + str(nrOfRuns) + " hyperparameter pick")
		C, runName = cnn_multiclass(gridSteps, epochs, savePath)

		ax1 = plt.subplot(221)
		plt.plot(C.history['acc'], label = runName)
		plt.title('Training')
		ax2 = plt.subplot(223)
		plt.plot(C.history['loss'], label = runName)
		ax3 = plt.subplot(222)
		plt.plot(C.history['val_acc'], label = runName)
		plt.title('Testing')
		ax4 = plt.subplot(224)
		plt.plot(C.history['val_loss'], label = runName)

		dictKey = f'Name{runName}TrainLoss'
		dictionary[dictKey] = C.history['loss']

		dictKey = f'Name{runName}TrainAcc'
		dictionary[dictKey] = C.history['acc']

		dictKey = f'Name{runName}TestLoss'
		dictionary[dictKey] = C.history['val_loss']

		dictKey = f'Name{runName}TestAcc'
		dictionary[dictKey] = C.history['val_acc']




		




	ax1.set_ylabel('Accuracy')
	ax2.set_ylabel('Loss')
	#ax1.set_xlabel('Epochs')
	ax2.set_xlabel('Epochs')
	#ax3.set_ylabel('Accuracy')
	#ax4.set_ylabel('Loss')
	#ax3.set_xlabel('Epochs')
	ax4.set_xlabel('Epochs')
	#ax1.legend()
	#ax2.legend()
	#ax3.legend()
	ax4.legend(loc='upper right', prop={'size': 6})


	plotName = f'acc_lossPlots{repeat+1}.png'
	plt.savefig(os.path.join(savePath,(plotName)), dpi = 300)
	plt.clf()
	print("Saving plots in " + savePath + "\n")

	print("Ending sweep [" + str(repeat+1) + "] out of " + str(nrOfLoops) + " hyperparameter sweeps \n")



	print("__________end_________")

#out of loop
	
with open(os.path.join(savePath,('dictionary.json')) , 'w+') as f:
	json.dump(dictionary, f)
 