
# Import Visualization Tools 
# pip install git+https://github.com/raghakot/keras-vis.git
from vis.losses import ActivationMaximization
from vis.regularizers import TotalVariation, LPNorm
from vis.input_modifiers import Jitter
from vis.optimizer import Optimizer
from vis.callbacks import GifGenerator
from keras.applications.vgg16 import VGG16
from vis.utils import utils
from matplotlib import pyplot as plt
from vis.visualization import visualize_activation
from vis.visualization import visualize_saliency
from keras import activations
import pdb
from keras.models import load_model

# Build the VGG16 network with ImageNet weights
#model = VGG16(weights='imagenet', include_top=True)
#print('Model loaded.')

yogai_model = load_model('my_model_multiclass10.h5')
print('Model loaded.')

# Seed Images to monitor 
seed_input = utils.load_img('test_set/downwarddog/File93.jpg', target_size=(64, 64))

# Downward Dog Class #
filter_idx = 3 

# Utility to search for layer index by name. 
# Alternatively we can specify this as -1 since it corresponds to the last layer.
layer_idx = utils.find_layer_idx(yogai_model, 'prediction')

# Swap softmax with linear
yogai_model.layers[layer_idx].activation = activations.linear
yogai_model = utils.apply_modifications(yogai_model)


#Look at Activation
img_a = visualize_activation(yogai_model, layer_idx)
plt.imshow(img_a)
plt.imsave("viz_activation.jpg",img_a)

#Look at Saliency Map
img_s = visualize_saliency(yogai_model, layer_idx, filter_indices = filter_idx, seed_input = seed_input)
plt.imshow(img_s)
plt.imsave("viz_saliency.jpg",img_s)