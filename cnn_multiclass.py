import keras
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from IPython.display import display 
import matplotlib.pyplot as plt
from matplotlib.pyplot import imshow
from PIL import Image
from random import *
import h5py
import numpy as np
import json
import os
import time
import pdb
from recall import *

def cnn_multiclass(gridSteps, epochs, higherSavePath):

  tic = time.clock()
  

  # Hyperparams ranges
  #gridSteps = 100;

  #alphaRange =  np.linspace(-4,-1, gridSteps) 
  #alphaRange =  np.linspace(-4,-2, gridSteps) #alpha = 0.9x
  #betaRange = np.linspace(-3, -1, gridSteps)
  betaRange = np.linspace(-1, -1, gridSteps) #Beta = 0.9
  #filterPick = [3,5,7]
  filterPick = [3]
  #miniBatchRange = [16, 32, 64]
  miniBatchRange = [16]
  denseNodesRange = [10, 20, 30, 32, 40, 50 , 60, 64, 70, 80, 90, 100, 110, 120, 128, 150 , 200, 256]
  #denseNodesRange = [128]
  #decayRange = np.linspace(10e-3, 10e-2, gridSteps)
  decayRange = [1]

  # Hyperparams random values between (0 and 1)*gridSteps

  #alphaPick = 10**(alphaRange[randint(0,gridSteps-1)])
  alphaPick = 0.0033
  betaPick = 1-10**(betaRange[randint(0,gridSteps-1)])
  filterPick= filterPick[randint(0,len(filterPick)-1)]
  miniBatchPick = miniBatchRange[randint(0,len(miniBatchRange)-1)]
  addConvLayers = [np.random.uniform(0,1) , 
                    np.random.uniform(0,1)]

  denseNodesPick = round(denseNodesRange[randint(0,len(denseNodesRange)-1)])
  decayPick = decayRange[randint(0,len(decayRange)-1)]





  # CNN model 
  classifier = Sequential()

  classifier.add(Conv2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))
  classifier.add(MaxPooling2D(pool_size = (2, 2)))
  classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
  classifier.add(MaxPooling2D(pool_size = (2, 2)))

  # Possibly add in extra layers 
  convCount = 2
  if addConvLayers[0] > 0.5 :
    classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    convCount += 1

  if addConvLayers[1] > 0.5 :
    classifier.add(Conv2D(32, (3, 3), activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))
    convCount += 1




  classifier.add(Flatten())
  classifier.add(Dense(units = 128, activation = 'relu'))
  classifier.add(Dense(units = 11, activation = 'softmax')) # number of classes 

  classifier.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])



  print("Compiling new network with properties: " 
    + "\n learning rate: " + str(alphaPick) 
    + "\n beta: " + str(betaPick) 
    + "\n Conv filter size: " + str(filterPick) 
    + "\n mini batch size: " + str(miniBatchPick) 
    + "\n number of conv layers " + str(convCount) 
    + "\n number of nodes in dense layer: " + str(denseNodesPick) 
    + "\n Lr decay rate (per epoch) " + str(decayPick)
    + "\n")
    
    


  # We want to check 
  # Precision, Recall, F1 score



  # Specify folder for saving - based on model architecture
  #higher_folder = 'saveFolder'
  #sub_folder = 'Outputs'


  filename = f'Dl{denseNodesPick}Lr{alphaPick}Cl{convCount}'
  saveFolderName = os.path.join(higherSavePath, filename)

  savePath = os.path.join(os.getcwd(),saveFolderName)
  print("Saving C.history in " + savePath + "\n")
  if not os.path.exists(savePath):
      os.makedirs(savePath)

  # Data Augmentation
  batch_size = miniBatchPick
  train_datagen = ImageDataGenerator(rescale = 1./255,
                                     shear_range = 0.2,
                                     zoom_range = 0.2,
                                     rotation_range = 20,
                                     horizontal_flip = True)

  test_datagen = ImageDataGenerator(rescale = 1./255)



  training_set = train_datagen.flow_from_directory('training_set',
                                                   target_size = (64, 64),
                                                   batch_size = 32,
                                                   class_mode = 'categorical')


  


  test_set = test_datagen.flow_from_directory('test_set',
                                              target_size = (64, 64),
                                              batch_size = 1,
                                              class_mode = 'categorical')

  # Set optimizer
  optim = optimizers.Adam(lr = alphaPick, beta_1 = betaPick, decay = decayPick)

  C = classifier.fit_generator(training_set,
                           steps_per_epoch = (822-58) // batch_size, # number of training set images, 729 #alai: actually found 730
                           epochs = epochs,
                           validation_data = test_set,
                           validation_steps = (119-12) // 1) # number of test set images, 229 #alai: actually found 109
  classifier.save(os.path.join(savePath,('my_model_multiclass10.h5'))) #save model 

  predictions = classifier.predict_generator(test_set, steps= (119-12) // 1)

  toc = time.clock()

  run_time = toc - tic
  '''
  y_pred_orig = predictions
  print(y_pred_orig[0])
  y_pred = np.rint(predictions)
  y_pred = np.asarray(y_pred, dtype = float)
  y_true = test_set.classes
  y_true = np.reshape(y_true, (119, 1))
  pdb.set_trace()

  #Calculate recall
  recallValue = recall(y_true, y_pred)

  pdb.set_trace()
  '''

  #Add run-time to dictionary
  C.history['run-time'] = run_time
  #C.history['recall'] = recallValue

  with open(os.path.join(savePath,('history.json')) , 'w+') as f:
    json.dump(C.history, f)

  return (C, filename)

'''
  plt.figure(1)
  plt.plot(C.history['acc'])
  plt.savefig(os.path.join(savePath,('accPlot.png')), dpi = 300)


  plt.figure(2)
  plt.plot(C.history['loss'])
  plt.savefig(os.path.join(savePath,('lossPlot.png')), dpi = 300)
  with open(os.path.join(savePath,('history.json')) , 'w+') as f:
    json.dump(C.history, f)

'''

