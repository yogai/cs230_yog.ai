# CS230 Project: Yog.ai

Our project involves the combination of an open source pose recognition system and a convolution neural network to classify yoga poses based on their keypoints. 


## Pose Recognition: [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)

CMU's has developed a Windows CPU portable demo version of the software, which works (albeit slowly) for our needs for images and video. If run on a AWS server or other GPU we will achieve faster runtimes. 

Open Pose Resources:
[Output Descriptions](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md)
[Demo Usage/Flags](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/demo_overview.md)


## Yoga Pose Classification: Baseline Model

The baseline model and data set is based of [this model](https://github.com/amarchenkova/yoga-pose-CNN/) from Marchenkova built in Keras.

The data set currently organized as follows as gathered by Marchenkova ([train](https://drive.google.com/drive/folders/1vsBgnEf2NcQNvlCnIZr0P1m2_XXLhLlQ), [dev](https://drive.google.com/drive/folders/1WW7n4C01nBgBGNsQBLiRrsTB7h0WFIOB)), with our addition of a "random" no pose class with images of random people from [TU Graz](http://www-old.emt.tugraz.at/~pinz/data/GRAZ_01/30220071_Reprint.pdf)

```
training_set/
    bridge/
        File1.jpg
        ...
    mountain/
        File1.jpg
        ...
    random/
        File1.jpg
        ...    
    ...
dev_set/
    bridge/
        File1.jpg
        ...
    mountain/
        File1.jpg
        ...
  	random/
        File1.jpg
        ...  
    ...
```


## Running the Model

While the model can be simply run on an image data set, we have also created function which actuates the computer webcam, runs it through the OpenPose network and writes it to a folder which the model can read from. Next steps would be to make this process more cleanly run through OpenPose and our updated model. 

## Web Cam + OpenPose

Scripts to interface the demo OpenPose with the Webcam: 

genPoseEstimations.py is a script that does the following:

```
- runs webcam.py script
    - open user's webcam (using OpenCV)
    - takes n (number of) pictures
    - saves these in a local directory

- runs OpenPoseDemo.exe on this directory through the local CMD window
- saves the output files
```


