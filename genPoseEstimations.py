
import cv2
import os
from datetime import datetime 
import numpy as np
import time
import subprocess

#Import own files
from webcam import *

#Settings
noBlend = True


#Set path and create folder
dt = datetime.now().strftime('Day%m_%d_Time%H_%M_%S')


#picFolderName = 'InputFolder\CapturedFrames' + dt
picFolderName = 'InputFolder\TestFrames'

picPath = os.path.join(os.getcwd(),(picFolderName))
print("Saving images in " + picPath + "\n")
if not os.path.exists(picPath):
    os.makedirs(picPath)


#outputPathName = 'OutputFolder\OutputFiles' + dt
outputPathName = 'OutputFolder\TestOutput'
outputPath = os.path.join(os.getcwd(),(outputPathName))
if not os.path.exists(outputPath):
    os.makedirs(outputPath)


#Takes pictures 
numberOfPics = 0
webcam(numberOfPics, picPath) #Run Webcam for 5 pics and saves


#Make call in cmd window


cmd_string= "openpose\Bin\OpenPoseDemo.exe --image_dir "\
+ picFolderName +" --write_images " + outputPathName +\
" --write_keypoint " + outputPathName 

if noBlend == True:
	cmd_string +=  " --disable_blending"
#print(cmd_string)

#subprocess.call('cd openpose')

subprocess.call(cmd_string)
